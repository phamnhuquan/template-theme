{
    'name': 'Mobifone Theme',
    'description': 'Mobifone Theme',
    'category': 'Theme/Services',
    'summary': 'Mobifone, Event',
    'sequence': 260,
    'version': '1.0.0',
    'author': 'MobifoneIT - PTPM3',
    'depends': ['theme_common'],
    'data': [
        'data/ir_asset.xml',
        'views/images.xml',
        'views/customizations.xml',
    ],
    'images': [
        'static/description/mobifone_description.png',
        'static/description/mobifone_screenshot.jpeg',
    ],
    'images_preview_theme': {
        'website.s_cover_default_image': '/theme_mobifone/static/src/img/snippets/s_cover.jpg',
        'website.s_media_list_default_image_1': '/theme_mobifone/static/src/img/snippets/s_media_list_1.jpg',
        'website.s_media_list_default_image_2': '/theme_mobifone/static/src/img/snippets/s_media_list_2.jpg',
        'website.s_media_list_default_image_3': '/theme_mobifone/static/src/img/snippets/s_media_list_3.jpg',
        'website.s_text_image_default_image': '/theme_mobifone/static/src/img/snippets/s_text_image.jpg',
        'website.s_three_columns_default_image_1': '/theme_mobifone/static/src/img/snippets/library_image_11.jpg',
        'website.s_three_columns_default_image_2': '/theme_mobifone/static/src/img/snippets/library_image_13.jpg',
        'website.s_three_columns_default_image_3': '/theme_mobifone/static/src/img/snippets/library_image_07.jpg',
        'website.library_image_03': '/theme_mobifone/static/src/img/snippets/library_image_03.jpg',
        'website.library_image_10': '/theme_mobifone/static/src/img/snippets/library_image_10.jpg',
        'website.library_image_13': '/theme_mobifone/static/src/img/snippets/library_image_23.jpg',
        'website.library_image_02': '/theme_mobifone/static/src/img/snippets/library_image_05.jpg',
        'website.library_image_14': '/theme_mobifone/static/src/img/snippets/library_image_14.jpg',
        'website.library_image_16': '/theme_mobifone/static/src/img/snippets/library_image_16.jpg',
    },
    'snippet_lists': {
        'homepage': ['s_cover', 's_title', 's_text_block', 's_three_columns', 's_images_wall',
                     's_title', 's_media_list', 's_text_image'],
    },
    'license': 'LGPL-3',
    'live_test_url': 'https://theme-mobifone.odoo.com',
    'assets': {
        'website.assets_editor': [
            'theme_mobifone/static/src/js/tour.js',
        ],
    }
}
