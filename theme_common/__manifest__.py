{
    'name': 'MBF - Theme Common',
    'summary': 'MBF - Snippets Library',
    'description': 'MBF - Snippets library containing snippets to be styled in themes.',
    'category': 'Hidden',
    'version': '1.1',
    'depends': ['website'],
    'data': [
        'data/data.xml',
        'views/old_snippets/s_column.xml',
        'views/old_snippets/s_page_header.xml',
        'views/old_snippets/s_three_columns_circle.xml',
    ],
    'application': False,
    'license': 'LGPL-3',
}
